# Introduction
Overall Idea for the text report -

Can be run entirely in R and loaded fully into text mining DTM type structures.
For extra large corpora or corpora that require special steps, don't use this code.

```
project structure:

        README
	load_corpus.R
	lemmatize.R 
	clean.R 

	ldatune.R
	ldamodel.R 
	word_frequency.R
	bigrams.R
	sentiment_analysis.R
	_site.yml
        gen_report.sh 

	/website-files
	    index.Rmd
            wf.Rmd
            bigrams.Rmd
            sentiment.Rmd
            lda.Rmd

	results/
	site/
```

# Workflow

1. Take a set of texts and load them into a Simple Corpus object.
2. Apply some preprocessing (lemmatization, cleaning, filtering infrequent words).
3. Use ldatuning to determine the number of topics.
4. Use lda package to run the topic model based on interpretation of ldatuning results.
5. Compute word frequencies and bigrams for the corpus.
6. Compute average sentiment per document of the corpus.
7. Modify _site.yml file to only include the pages you want
8. Run get_site.sh script

## 1. Load text into Simple Corpus object

Modify load_corpus.R depending on the input data format.
For example, the input data may be in a csv or a set of text files.
Save the corpus object into ./results/corpus.rds

## 2. Preprocessing

### a. optional: lemmatization
Note: If you want to lemmatize the corpus, make sure to do that first, as steps such as getting rid of stop words may negatively impact the lemmatizer.
To lemmatize the corpus simply run the lemmatize.R script.

### b. cleaning
This step is configurable at the start of the clean.R script.
If you lemmatized the corpus first, it will use that as input.
Output is a dtm with sparse words removed, as well as a cleaned corpus object.

## 3. Tuning LDA

Takes as input the output of the cleaning step - a dtm with sparse words removed.
Runs ldatuning on the corpus, plots and saves the results.
Then you need to interpret the results to determine the number of topics to use for the topic model.

## 4. LDA 

Takes as input the output of the cleaning step - a dtm with sparse words removed.
Configure the parameters for the model at the top of the script.
Output is a topic terms matrix, doc topics matrix, and the json for LDAvis

## 5. Word Frequency and Bigrams

### a. word frequency
Uses the same non sparse dtm and sorts the words by frequency.

### b. bigrams
Uses the cleaned corpus object and computes the most frequent bigrams.

## 6. Sentiment Analysis
Takes as input the raw not processed corpus. 
For each document in that corpus, split into sentences and compute the sentiment polarity score.
Remove the outlier sentences.
Average the sentiments of the sentences for each document.
Compute some summary statistic of the sentences for each document.
Compute some summary statistics

## 7. Modify _site.yml 

Each step of the report is optional. 
cd to website-files
Edit the _site.yml file to not include pages you didnt run.
Prepend an '_' to the Rmd files for the pages you don't want to run.

## 8. Run get_site.sh script

Generates the site based on the _site.yml file.
Note that the ldavis output requires CORS to load the json.
Effectively this means that you won't be able to actually see the results unless you are viewing it on a web server.


